#!/usr/bin/env python3

"""deduplicate.py: FortiOS duplicate policy finder"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.1"
__status__ = "Production"

import os
import argparse

def uniq(lst):
    last = object()
    for item in lst:
        if item == last:
            continue
        yield item
        last = item


def sort_and_deduplicate(l):
    return list(uniq(sorted(l)))


def printnumlines(lst, s=0):
    for n, item in enumerate(lst):
        print(str(n+s) + ' - ' + str(item))


def hasvdoms():
    ans = 0
    for _ in cfg:
        if _ == 'config vdom\n':
            ans = 1
            break
    return ans


def dosyntax():
    if hasvd:
        print('config vdom\nedit ' + vdom + '\nconfig firewall policy')
    else:
        print('config firewall policy')
    for d in dupelist:
        for dupe in d[1:]:
            print('delete ' + str(dupe))
    if hasvd:
        print('end\nend\n')
    else:
        print('end\n')


BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal
os.system('cls' if os.name == 'nt' else 'clear')

print(GREEN + '\n\n\n\n\nWelcome!\nThis is FortiOS Duplicate Policy Finder v1.0\n' + RED + 'Run "deduplicate.py -h" for help with options\n' + NORM)
# adding command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--config', help='Print ready to copy/paste config to delete duplicates', action='store_true')
args = parser.parse_args()


# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .conf to list "cfgfiles"
for file in dirs:
    if ".conf" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
else:
    print('\nOnly one config file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()
if args.config:
    print('\nSyntax to delete duplicate policies from ' + cfgfile + ':\n')
else:
    print('\nDuplicate policies in ' + cfgfile + ':\n')

# slicing config file to start with 'config global\n' which will ease the vdom separation

hasvd = hasvdoms()
if hasvd:
    confstartindex = 0
    for num, line in enumerate(cfg):
        if line == 'config global\n':
            confstartindex = num
    cfg = cfg[confstartindex:]

# separating policy parts of config file
# finding first line of policy configuration part
policystartindex = []
policyendindex = []
vdomindex = []


for num, line in enumerate(cfg):
    if hasvd:
        if line == 'config vdom\n':
            vdomindex.append(num)
    if line == 'config firewall policy\n':
        policystartindex.append(num)

# finding last line of policy configuration part
for index in policystartindex:
    for num, line in enumerate(cfg):
        if line == 'end\n' and num > index:
            policyendindex.append(num)
            break

for count, ind in enumerate(policystartindex):
    if hasvd:
        vdom = (cfg[vdomindex[count] + 1][5:].strip('\n'))
        if args.config:
            pass
        else:
            print('\nVDOM ' + vdom)

    start = policystartindex[count]
    end = policyendindex[count]

    # assigning policy table slice to a list
    policytable = cfg[start:end + 1]
    policytable = [line for line in policytable if 'set uuid' not in line]
    policytable = [line for line in policytable if 'set comment' not in line]
    # slicing policytable
    polstart = []
    polend = []
    temppoltable = []
    poltable = []

    # finding first lines of single policy config
    for num, line in enumerate(policytable):
        if line.startswith('    edit '):
            polstart.append(num)

    # finding last lines of single policy config
    for num, line in enumerate(policytable):
        if line.startswith('    next'):
            polend.append(num)

    # deduplicating and printing the results
    for num, line in enumerate(polstart):
        poltable.append(policytable[polstart[num]:polend[num]])
    dupelist = []
    for l1 in poltable:
        dupes = []
        for l2 in poltable:
            if l2[1:] == l1[1:]:
                dupes.append(l2[0][9:].strip('\n'))
            if len(dupes) > 1:
                dupelist.append(dupes)
    dupelist = sort_and_deduplicate(dupelist)


    if args.config:
        dosyntax()
    else:
        printnumlines(dupelist, 1)
print('\n\n')
