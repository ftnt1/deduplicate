# Changelog: #

## 1.1 ##

### Fixed ###

- Now it properly cleans the terminal before run

### Changed ###

- It displays "VDOM" before VDOM name now

## 1.0 ##
Initial release