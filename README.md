# README #

FortiOS duplicate policy finder

## Files ##

### deduplicate.py ###

This will find duplicate policies within a config file (it skips UUID's, comments).

Run it from within the directory containing firewall config(s), then follow instructions on screen.

Without arguments it will display duplicate policy ID's in their respective VDOM's

### README.md ###

This file

## Usage ##

deduplicate.py [-h] [-c]

Without arguments it will just list all VDOM's (if any) and display identical firewall policy ID's

optional arguments:

  -h, --help    show this help message and exit

  -c, --config  Print ready to copy/paste config to delete duplicates

### Example: ###

In config file you have some VDOM's and in one of them you have something like this:

```
config firewall policy
    edit 1
        set uuid 448a8442-07d4-51e7-6397-f0c576b09a6d
        set srcintf "vxlan"
        set dstintf "port22"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
    edit 2
        set uuid 4956d9bc-07d4-51e7-f39a-ed0e99f843a4
        set srcintf "port22"
        set dstintf "vxlan"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
    edit 3
        set uuid 4956d9bc-0000-0000-0000-ed0e99f843a4
        set srcintf "port22"
        set dstintf "vxlan"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
end
```

Run script without parameters - it will display:

```
Welcome!
This is FortiOS Duplicate Policy Finder v1.0
Run "deduplicate.py -h" for help with options

0 - GLAR01-fw01_20170824_1317_orig.conf

Only one config file found in current directory, I will use it

Reading file: GLAR01-fw01_20170824_1317_orig.conf


Done!


Duplicate policies in GLAR01-fw01_20170824_1317_orig.conf:


VDOM root
1 - ['2', '3']

VDOM technik

VDOM gast

VDOM intern

VDOM transport

VDOM extern
```

Run it with '-c' and you will see:

```
Welcome!
This is FortiOS Duplicate Policy Finder v1.0
Run "deduplicate.py -h" for help with options

0 - GLAR01-fw01_20170824_1317_orig.conf

Only one config file found in current directory, I will use it

Reading file: GLAR01-fw01_20170824_1317_orig.conf


Done!


Duplicate policies in GLAR01-fw01_20170824_1317_orig.conf:


VDOM root
1 - ['2', '3']

VDOM technik

VDOM gast

VDOM intern

VDOM transport

VDOM extern
```